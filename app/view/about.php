	<div id="content">
		<div class="row">
			<div class="image">
				<img src="public/images/content/inner1.jpg" alt="Elder Guy Smiling">
			</div>
			<!-- <div id="about-contact-photos">
				<div class="row">
					<div class="acTop">
						<img src="public/images/content/about-contact/6.jpg" alt="" class="img1">
						<img src="public/images/content/about-contact/5.jpg" alt="" class="img2">
					</div>
					<div class="acBot">
						<img src="public/images/content/about-contact/1.jpg" alt="">
						<img src="public/images/content/about-contact/2.jpg" alt="">
						<img src="public/images/content/about-contact/3.jpg" alt="">
						<img src="public/images/content/about-contact/4.jpg" alt="">
					</div>
				</div>
			</div> -->
			<h1>About Us</h1>
			<p class="about-desc">Amari Trenellbre Lifestyle Mgt., LLC is an independently owned and operated limited liability company dedicated to bringing cost-efficient home care solutions to individuals with limitations on daily activities. We off a wide variety of in-home care services to maintain the overall quality of life and independence of our clients and their caregivers. Our services are tailored to fit your individual needs; our flexibility allows us to ensure each client receives the care and respect that they expect and deserve. We not only focus on the patients we serve but the loved ones who care for them. We understand the challenges that may occur while dealing with a loved one who has become limited on daily activities. Our goal is to help you or your loved one remain as independent as possible so that you can enjoy a happy and healthy lifestyle within the comfort and convenience of your own home.</p>
		</div>
	</div>
