<?php if($view == "home"):?>
<!-- content bottom -->
<div id="contact-section">
	<div class="row">
		<div class="top-text">
			<p>If you're interested in joining our team we would love to hear from you. <br>
			Employee Benefits also available.</p>
			<br>

			<p><small><img src="public/images/common/phoneBG-big.png" alt=""></small> <?php $this->info(["phone","tel"]); ?></p>
		</div>

		<h2>Contact Us</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<label><span>Name:</span>
				<input type="text" name="name" placeholder="Name:">
			</label>
			<label><span>Phone:</span>
				<input type="text" name="phone" placeholder="Phone:">
			</label>
			<label><span>Email:</span>
				<input type="text" name="email" placeholder="Email:">
			</label>
			<label><span>Message:</span>
				<textarea name="message" placeholder="Message:"></textarea>
			</label>
			<div class="cap-cover">
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
			</div>
			<label>
				<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
			</label><br>
			<?php if( $this->siteInfo['policy_link'] ): ?>
			<label>
				<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
			</label>
			<?php endif ?>
			<button type="submit" class="ctcBtn" disabled>Submit Form</button>
		</form>
	</div>
</div>

<div id="gallery-section">
	<div class="row">
		<ul>
			<li><img src="public/images/content/gallery-list-img1.jpg" alt=""></li>
			<li><img src="public/images/content/gallery-list-img2.jpg" alt=""></li>
			<li><img src="public/images/content/gallery-list-img3.jpg" alt=""></li>
			<li><img src="public/images/content/gallery-list-img4.jpg" alt=""></li>
		</ul>

		<!-- company logo -->
		<p class="logo"><a href="home"><img src="public/images/common/logo.png" alt=""></a></p>
		<!-- end -->

		<p><small><img src="public/images/common/emailBG.png" alt=""></small>
		<?php $this->info(["email","mailto"]); ?></p>

		<p><small><img src="public/images/common/numberBG.png" alt=""></small>
		<?php $this->info(["phone","tel"]); ?></p>

		<p><small><img src="public/images/common/addressBG.png" alt=""></small>
		<?php $this->info("address"); ?></p>
	</div>
</div>
<!-- end -->
<?php endif; ?>

<!-- footer -->
<footer>
	<div class="row">
		<nav>
			<ul>
				<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
				<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">About Us</a></li>
				<li <?php $this->helpers->isActiveMenu("mission"); ?>><a href="<?php echo URL ?>mission#content">Our Mission</a></li>
				<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
				<li <?php $this->helpers->isActiveMenu("our-team"); ?>><a href="<?php echo URL ?>our-team#content">Our Team</a></li>
				<li <?php $this->helpers->isActiveMenu("join-team"); ?>><a href="<?php echo URL ?>join-team#content">Join Team</a></li>
				<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact Us</a></li>
			</ul>
		</nav>

		<p class="copy">&copy; <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?>. All Rights Reserved. <?php if( $this->siteInfo['policy_link'] ): ?>
				<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>. 
			<?php endif ?></p>
		<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank"> Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
	</div>
</footer>
<!-- end -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>


