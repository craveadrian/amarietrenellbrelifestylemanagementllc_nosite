<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
<header>
	<div class="row">
		<nav>
			<a href="#" id="pull">Menu</a>
			<ul>
				<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
				<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">About Us</a></li>
				<li <?php $this->helpers->isActiveMenu("mission"); ?>><a href="<?php echo URL ?>mission#content">Our Mission</a></li>
				<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
				<li <?php $this->helpers->isActiveMenu("our-team"); ?>><a href="<?php echo URL ?>our-team#content">Our Team</a></li>
				<li <?php $this->helpers->isActiveMenu("join-team"); ?>><a href="<?php echo URL ?>join-team#content">Join Team</a></li>
				<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact Us</a></li>
			</ul>
		</nav>

		<div class="text">
			<p><small><img src="public/images/common/phoneBG.png" alt=""></small> <?php $this->info(["phone","tel"]); ?></p>
		</div>
	</div>
</header>

<?php if($view == "home"):?>
<!-- banner -->
<div id="banner">
	<div class="row">
		<!-- company logo -->
		<p class="logo"><a href="home"><img src="public/images/common/logo.png" alt=""></a></p>
		<!-- end -->
		<p><a href="contact#content" class="button">Contact us today!</a></p>
	</div>
</div>

<div id="services-section">
	<div class="row">
		<dl>
			<dt><img src="public/images/content/services-dt1.jpg" alt=""></dt>
			<dd><small>Trust our caregivers</small>
				<p>Our caregiver's goal is to provide you and your loved ones the quality, compassion, and comfort that you expect and deserve. Our patient centered services are designed around your schedule and individual needs.</p>
				</dd><br class="clear">
		</dl>

		<dl class="alter-data">
			<dt><img src="public/images/content/services-dt2.jpg" alt=""></dt>
			<dd><small>We ensure safety</small>
				<p>We are available 24/7. We offer long distance family members regular well being checks on their loved ones to ensure that they are being cared for, treated fairly, and safe no matter where they live. Home safety evaluations and recommendations to reduce fall risk are also included in your care plan.</p>
			</dd><br class="clear">
		</dl>

		<dl>
			<dt><img src="public/images/content/services-dt3.jpg" alt=""></dt>
			<dd><small>We help those in need</small>
				<p>We offer affordability without sacrificing quality of care. Our family owned business takes pride in being able to help those in need with day-to-day responsibilities, including personal care and housekeeping duties.</p></dd><br class="clear">
		</dl>
	</div>
</div>
<!-- end -->
<?php endif; ?>
