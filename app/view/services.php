	<div id="content">
		<div class="row">
			<div class="image">
				<img src="public/images/content/inner7.jpg" alt="Elder Guy Smiling" class="service">
				<img src="public/images/content/inner5.jpg" alt="Elder Guy Smiling" class="service">
				<img src="public/images/content/inner3.jpg" alt="Elder Guy Smiling" class="service">
			</div>
			<h1>Services</h1>
			<p class="services-desc">As your trusted in-home care provider you can expect a comprehensive care plan that is designed to fit your individual needs with the most cost-effective means possible. As part of our services to you, we work closely with your physician to carry out specific orders to go along with a complete plan of action and preventative solution to reduce the risk of unplanned hospital visits or added medical expenses.Each client is treated as if they are our own loved one.</p>
			<h3 class="home-care-solution-header">Home care solutions and services include the following:</h3>
			<ul class="list-home-care-services">
				<li>24/7 Emergency Support</li>
				<li>Safety and Wellbeing Check-Ups</li>
				<li>Home/Personal Care</li>
				<li>Meals Preparation</li>
				<li>Companionship</li>
				<li>Fall Prevention </li>
				<li>Social Service and Education</li>
				<li>Additional Resources and Support also available as needed.</li>
			</ul>
			<p class="last-parag-services">Home care services are usually the most cost-effective, most preferred method, and most convenient care option. We thrive to make our home care service your number one solution for to health care needs so that you can enjoy a Happy, Healthy Lifestyle. </p>
			<p class="tagline-services">It's your Lifestyle, Live It Up!!</p>
		</div>
	</div>
