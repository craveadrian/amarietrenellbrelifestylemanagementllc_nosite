<div id="content">
	<div class="row">
		<div class="text">
			<h1>Amari Trenellbre</h1>
			<h2><span>Lifestyle Management,</span> LLC.</h2>

			<ul>
				<li><img src="public/images/content/img1.jpg" alt=""></li>
				<li><img src="public/images/content/img2.jpg" alt=""></li>
			</ul>
		</div>

		<div class="right-list">
			<ul>
				<li>24/7 Service with Immediate Care</li>
				<li>Bathing, Dressing and Grooming</li>
				<li>Conversation and Companionship</li>
				<li>Grocery Shopping</li>
				<li>Laundry and Light Housekeeping</li>
				<li>Meal Preparation</li>
				<li>Medication Reminders</li>
				<li>Mobility and Exercise Assistance</li>
				<li>Monitoring Vital Signs</li>
				<li>Personal Care</li>
				<li>Memory Care</li>
				<li>Financial Services/CPA</li>
				<li>Additional Resources/Social Services</li>
				<li>And More</li>
			</ul>
		</div>
	</div>
</div>
