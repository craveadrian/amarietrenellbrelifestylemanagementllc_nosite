	<div id="content">
		<div class="row">
			<div class="image">
				<img src="public/images/content/inner8.jpg" alt="Elder Guy Smiling">
			</div>
			<h1>Join Our Team</h1>
			<p class="join-our-team">Thank you for your interest in joining the Amari Trenellbre Lifestyle Mgt., LLC. team. We have exciting news!!! We have partnered with a great Outsourcing Service to provide our employees with comprehensive benefits, human resources, administration services, and more! Our partnership with them will provide increased access to employee programs and services. We are very excited for the opportunity to bring these services to our employees and we are confident that you will be too. As a Company, in addition to our client's, our employees are also recognize as valued assets to our company. Amari Trenellbre Lifestyle Mgt., LLC. is an Equal Opportunity and I-9 Employer who loves a well diversed work place.We look forward to having you as part of our growing team.</p>
			<h2 class="join-our-team">What can you expect?</h2>
			<h3 class="join-our-team">COMPREHENSIVE PAYROLL SERVICES </h3>
			<p class="join-our-team">A dedicated Employee Services Website where you can access your pay stubs and the employee discount programs. The Employee Connect and Mobile App, will also be available to access select areas of the Employee Services we provide</p>
			<p class="join-our-team">Access to the Human Resources Services Center to discuss any employment concerns.</p>
			<p class="join-our-team">Training opportunities through the eLearning center – you can take advantage of over 400 online training courses at no cost. Your manager will provide details regarding access.</p>
			<h3 class="join-our-team">EMPLOYEE DISCOUNT PROGRAM</h3>
			<p class="join-our-team">Additionally, we offer working advantage - numerous employee discount programs through our partnership. These include discounts on travel, retail, entertainment and more. </p>
			<p class="join-our-team">Some of these discounts include the following: </p>
			<ul class="join-our-team">
				<li>AT&T Wireless – up to 18% off the monthly service charge</li>
				<li>Costco – receive bonus coupons upon joining (new members only)</li>
				<li>Office Depot – discounts on a large selection of office supplies, school supplies, and copy/finishing services</li>
				<li>Home Point Financial – preferred mortgage program</li>
				<li>Pet Discounts – pet insurance, pet health discounts and pet medicine discounts</li>
				<li>DentaChoice – discounts on dental and vision services</li>
				<li>1-800-Flowers.com – 20% discount on flowers and gifts</li>
				<li>TicketsAtWork/Working Advantage – discounts on retail, travel, entertainment and movie tickets, plus rental cars</li>
				<li>BrooksBrothers.com – 15% off clothing</li>
				<li>Anytime Fitness- 10% off monthly fee/50% off enrollment</li>
				<li>WMPH – Cruise discounts</li>
				<li>And much more</li>
			</ul>
			<h3 class="join-our-team">ELECTRONIC ONBOARDING PROCESS</h3>
<!-- 			<p class="join-our-team">Steps for the Onboarding (flyer)</p> -->
			<!-- Oasis link -->
			<div style="width:300px; font-family: arial, helvetica, verdana; font-size: 12px;">


			<br />
			<a href="https://employee.oasispayroll.com/" title="Oasis Outsourcing - PEO, Payroll and Benefits"><img src="https://employee.oasispayroll.com/layout/images/oasis_logo_005696_400w.png" alt="Oasis Outsourcing logo" style="max-widthborder: none;"/></a>
			<a href="https://employee.oasispayroll.com/" title="Oasis Outsourcing - PEO, Payroll and Benefits">
			Click here to view or manage personalized employee benefits and payroll information.</a>
			<br /><br />

			</div>
				<a href="public/images/doc/Electronic onboarding flyer 18891.pdf" target="_blank"><img src="public/images/common/pdf.jpg" class="pdf-img"></a>
			<!-- Oasis link -->
			<p>To begin the New Hire Electronic Onboarding (EOB) process, please go to the following <br />website: <a href="http://www.oasisadvantage.com/onboarding" target="_blank">http://www.oasisadvantage.com/onboarding</a>.</p>
			<p>Once there, select “CLICK HERE”, located within the last sentence of the paragraph entitled LOCAL EMPLOYEES.</p><br />


			<!--<p>Attn Jenifer (Place the attached image here)</p>-->

			<p><u>The following are some useful tips and information for completing the EOB process:</u></p><br />
			<ul class="payroll">
			 <li>The Oasis client ID is <strong>OA18891</strong></li>
			 <li>The Employer PIN is <strong>73383</strong></li>
			 <li>Please do not use any characters when entering your Social Security Number, phone number or birth date.</li>
			 <li>For Direct Deposit, please have your bank routing and account number(s) available.</li>
			 <li>Please have your emergency contact number available</li>
			 <li>Please also have your w-4 filing status and withholding amount available</li>
			</ul>
<br />
			<p><strong>I-9 documentation should be current, and provided to your onsite manager at the time of inital interview for verification purposes</strong></p>
<br />
			<p>PLEASE COMPLETE THE NEW HIRE PAPERWORK (after interview)</p><br />

			<p> <u>Also, included in the New Hire Packet is the Core Handbook as well as information regarding some various services available to you through our partnership with Oasis, including:</u></p><br />
			<ul class="payroll">
				<li>The Employee Services Website, which includes—</li>
				<li>Access to your payroll and benefits information</li>
				<li>W-4 and Direct Deposit change forms</li>
				<li>Printable Check Stubs</li>
				<li>The Employee Discount Program</li>
				<li>eLearning and W-2 Services</li>
				<li>Mobile Access available through the Oasis Employee Connect app</li>
			</ul><br />

			<p><strong><i>For EOB technical support, please contact a representative at <?php $this->info(["phone2","tel"]); ?>.</i></strong></p><br />
			<p>Thank you for your time and attention. We look forward to working with you!</p>
		</div>
	</div>
