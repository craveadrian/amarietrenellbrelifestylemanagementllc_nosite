	<div id="content">
		<div class="row">
			<div class="image">
				<img src="public/images/content/inner4.jpg" alt="Elder Guy Smiling">
			</div>
			<h1>Our Team</h1>
      <p class="our-team-desc">Our growing team are readily available 24/7 to accommodate your individual needs. We work to establish and maintain ongoing partnerships with in the community to maximize our available resources and expand our services to those who may not otherwise have it. </p>
<ul class="strive-excell-list">
			<li>Strive for Excellence Model</li>

				<li>Each client is treated as if they are our own loved one.
</li>
				<li>Apply comprehensive knowledge of home health care so our clients are safe, independent, and comfortable.</li>
				<li>Treat our clients with dignity, respect, honesty, and compassion that they deserve.</li>
				<li>Always available, always fair while protecting their rights and privacy.</li>
				<li>Communicate with our clients and their family on a regular basis to ensure quality assurance.</li>
				<li>Actively involve clients in their own care by listening and catering to their needs.</li>
				<li>Coordinate our services with other health care providers to achieve maximum health and delivery of clients’ overall lifestyle goal.</li>
				<li>Delight our clients with attention to detail that exceeds their expectations and open to Special or custom request.</li>
				<li>Give clients trust and confidence in our abilities by utilizing skilled staff who are experts in their field.</li>
				<li>Our Nursing Staff and Operations Team are dedicated to bringing a strong level of management and direction to both our caregivers and our clients. </li>
				<li>Each of our providers are trained and experienced in early intervention practices to coordinate a safe work and living environment for an effective delivery of care. </li>
				<li>Our cutting-edge technological approach ensures accountability and a safer innovative approach in the health care industry through our software platform.</li>
			</ul>
			<!--<div id="gall1" class="gallery-container">
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "default") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["alt"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>-->
		</div>
	</div>
