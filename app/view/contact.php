<div id="content">
	<div class="row">
		<div class="image">
			<img src="public/images/content/inner6.jpg" alt="Elder Guy Smiling">
		</div>
		<!-- <div id="about-contact-photos">
			<div class="row">
				<div class="acTop">
					<img src="public/images/content/about-contact/6.jpg" alt="" class="img1">
					<img src="public/images/content/about-contact/5.jpg" alt="" class="img2">
				</div>
				<div class="acBot">
					<img src="public/images/content/about-contact/1.jpg" alt="">
					<img src="public/images/content/about-contact/2.jpg" alt="">
					<img src="public/images/content/about-contact/3.jpg" alt="">
					<img src="public/images/content/about-contact/4.jpg" alt="">
				</div>
			</div>
		</div> -->
		<h1>Contact Us</h1>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<label><span>Name:</span>
				<input type="text" name="name" placeholder="Name:">
			</label>
			<label><span>Address:</span>
				<input type="text" name="address" placeholder="Address:">
			</label>
			<label><span>Email:</span>
				<input type="text" name="email" placeholder="Email:">
			</label>
			<label><span>Phone:</span>
				<input type="text" name="phone" placeholder="Phone:">
			</label>
			<label><span>Message:</span>
				<textarea name="message" placeholder="Message:"></textarea>
			</label>
			<div class="cap-cover">
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
			</div>
			<label>
				<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
			</label><br>
			<?php if( $this->siteInfo['policy_link'] ): ?>
			<label>
				<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
			</label>
			<?php endif ?>
			<button type="submit" class="ctcBtn" disabled>Submit Form</button>
		</form>
	</div>
</div>
